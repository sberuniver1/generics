package ru.edu;


import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        List<A> oneCollection = new ArrayList<>();
        oneCollection.add(new A());
        oneCollection.add(new A());

        List<B> secondCollection = new ArrayList<>();
        secondCollection.add(new B());
        secondCollection.add(new B());

        List<? super A> list = oneCollection;
        foo(list);
    }

    private static void foo(List<? super B> list) {
        // теперь в качестве аргумента может прийти коллекция элементов А, B или Object.
        // мы можем добавить в эту коллекцию наследников от B
        // создали коллекцию, которую нельзя читать, но в ней можно менять данные (Consumer)
        list.add(new B());

        // а прочитать данные явно мы не сможем, так как мы не знаем, каким именно типом данных типизирована коллекция
        //B value = list.get(1);

        // единственный вариант, как мы можем прочитать данные - использовать Object
        Object value = list.get(1);
    }

}
