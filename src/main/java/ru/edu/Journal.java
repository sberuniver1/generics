package ru.edu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Journal {
    private List<User> students = new ArrayList<>();

    // метод добавления пользователей в коллекцию
    public void add(User user) {
        students.add(user);
    }

    // метод, возвращающий экземпляр итератора по пользователям
    public Iterator<User> getUserIterator() {
        return new JournalIterator();
    }

    /*
    Чтобы иметь возможность итерироваться по классу Journal,
    создадим внутренний класс JournalIterator, который будет имплементировать
    интерфейс Iterator
     */
    public class JournalIterator implements Iterator<User> {
        private int position;

        // конструктор, который создает объект итератора
        public JournalIterator() {
            this.position = 0;
        }

        @Override
        public boolean hasNext() {
            // если позиция меньше, чем размер списка студентов, то есть еще по чему итерироваться
            // обращаемся из внутреннего класса к переменной students внешнего класса
            if (position < students.size()) {
                return true;
            } else {
                return false;
            }
        }

        @Override
        public User next() {
            // необходимо вернуть текущий элемент, на который ссылается итератор, и сдвинуться правее
            User user = students.get(position);
            position += 1;
            return user;
        }
    }
}
