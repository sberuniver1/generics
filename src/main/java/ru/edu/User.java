package ru.edu;

public class User {
    private String id;
    private String firstName;
    private String lastName;
    private String secondName;
    private String phone;
    private int age;
    private String role;

    // метод, возвращающий экземпляр класса Builder
    public static Builder builder() {
        return new Builder();
    }

    // статический вложенный класс
    public static class Builder {
        private User obj = new User();

        // указываем поля, которые хотим проинициализировать
        public Builder setId(String id) {
            obj.id = id;
            return this;
        }

        public Builder setFirstName(String firstName) {
            obj.firstName = firstName;
            return this;
        }

        public Builder setSecondName(String secondName) {
            obj.secondName = secondName;
            return this;
        }

        public Builder setLastName(String lastName) {
            obj.lastName = lastName;
            return this;
        }

        public Builder setPhone(String phone) {
            obj.phone = phone;
            return this;
        }

        public Builder setAge(int age) {
            obj.age = age;
            return this;
        }

        public Builder setRole(String role) {
            obj.role = role;
            return this;
        }

        // метод build, который возвращает тип User
        public User build() {
            // проверка обязательных полей
            if (obj.firstName == null || obj.lastName == null) {
                throw new IllegalArgumentException("First name of Last name are required");
            }
            return obj;
        }
    }

    // геттеры всех полей
    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getPhone() {
        return phone;
    }

    public int getAge() {
        return age;
    }

    public String getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", phone='" + phone + '\'' +
                ", age=" + age +
                ", role=" + role +
                '}';
    }
}
